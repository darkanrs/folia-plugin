package com.darkan

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.websocket.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.websocket.*
import io.papermc.paper.event.player.AsyncChatEvent
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.consumeEach
import kotlinx.serialization.json.*
import net.kyori.adventure.text.serializer.plain.PlainTextComponentSerializer
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.plugin.java.JavaPlugin
import java.sql.DriverManager
import java.sql.SQLException
import java.util.*
import kotlin.coroutines.CoroutineContext

class DarkanFoliaPlugin : JavaPlugin(), CoroutineScope {
    private val dataDbUrl = "jdbc:sqlite:${dataFolder.path}/data.db"
    private lateinit var job: Job
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    private val authClient = HttpClient(CIO) {
        install(ContentNegotiation) {
            json()
        }
    }

    private val sessions: MutableMap<UUID, Pair<HttpClient, DefaultClientWebSocketSession>> = mutableMapOf()

    companion object {
        fun getPlugin(): DarkanFoliaPlugin {
            return getPlugin(DarkanFoliaPlugin::class.java)
        }
    }

    class DarkanEventListener : Listener {
        init { Bukkit.getPluginManager().registerEvents(this, getPlugin()) }

        @EventHandler(priority = EventPriority.MONITOR)
        fun onPlayerQuit(event: PlayerQuitEvent) {
            getPlugin().quit(event.player)
        }

        @EventHandler(priority = EventPriority.MONITOR)
        fun onPlayerJoin(event: PlayerJoinEvent) {
            getPlugin().join(event.player)
        }
    }

    override fun onEnable() {
        job = Job()
        saveDefaultConfig()
        DarkanEventListener()
        launch {
            withContext(Dispatchers.IO) {
                initializeDatabase()
            }
        }
        logger.info("DarkanFoliaPlugin has been enabled!")
    }

    override fun onDisable() {
        authClient.close()
        sessions.values.forEach { it.first.close() }
        logger.info("DarkanFoliaPlugin has been disabled.")
    }

    override fun onCommand(player: CommandSender, command: Command, label: String, args: Array<String>): Boolean {
        if (player !is Player)
            return false
        when (label) {
            "login" -> {
                if (args.size < 2) {
                    player.sendMessage("§cUsage: /login <username_(with_underscores_instead_of_spaces)> <password (can have spaces)>")
                    return true
                }
                launch {
                    val username = args[0].lowercase()
                    val password = args.drop(1).joinToString(separator = "")

                    val loginResponse = post("https://${config.getString("apiServer")}/v1/accounts/login") {
                        contentType(ContentType.Application.Json)
                        setBody(mapOf("username" to username, "password" to password))
                    }
                    if (!loginResponse.status.isSuccess()) {
                        player.sendMessage("§cLogin failed. Please check your credentials.")
                        return@launch
                    }
                    val jwt = loginResponse.body<JsonObject>()["access_token"]?.jsonPrimitive?.contentOrNull
                    if (jwt == null) {
                        player.sendMessage("§cFailed to obtain access token.")
                        return@launch
                    }
                    setAuthToken(player.identity().uuid().toString(), jwt)
                    connectToWebsocket(player, jwt)
                }
                return true
            }
            "fc" -> {
                val client = sessions[player.identity().uuid()]?.second
                if (client == null || !client.isActive) {
                    player.sendMessage("§cYou aren't signed into your account. Please use the /login command to sign into your Darkan account.")
                    return false
                }
                launch {
                    val text = args.joinToString(separator = " ")
                    client.send("""{ "event": "fc-message", "data": { "message": "$text" } }""")
                }
                return true
            }
        }
        return false
    }

    private suspend fun connectToWebsocket(player: Player, existingJwt: String? = null) {
            try {
                val jwt = existingJwt ?: getAuthToken(player.identity().uuid().toString()) ?: return player.sendMessage("§3Darkan §f- Authorization not found. Please login using /login to chat in your Darkan Friends Chat.")
                val playerResponse = get("https://${config.getString("apiServer")}/v1/accounts") {
                    header("Authorization", "Bearer $jwt")
                }
                if (!playerResponse.status.isSuccess()) {
                    player.sendMessage("§cAuth token expired or invalid. Please /login again.")
                    return
                }
                val account = playerResponse.body<JsonObject>()
                val existing = sessions[player.identity().uuid()]
                if (existing == null) {
                    val client = HttpClient(CIO) {
                        install(WebSockets) {
                            contentConverter = KotlinxWebsocketSerializationConverter(Json)
                            pingInterval = 20_000
                        }
                    }
                    val session = client.webSocketSession("wss://${config.getString("apiServer")}/v1/social/ws") {
                        header("Authorization", "Bearer $jwt")
                    }
                    sessions[player.identity().uuid()] = Pair(client, session)
                    session.async {
                        session.incoming.consumeEach { frame ->
                            if (frame !is Frame.Text) return@consumeEach
                            try {
                                val message = Json.decodeFromString<JsonObject>(frame.readText())
                                when(message["type"]?.jsonPrimitive?.contentOrNull) {
                                    "FCMessageEvent" -> {
                                        val fcName = message["data"]?.jsonObject?.get("fcOwner")?.jsonPrimitive?.contentOrNull
                                        val senderName = message["data"]?.jsonObject?.get("srcName")?.jsonPrimitive?.contentOrNull
                                        val text = message["data"]?.jsonObject?.get("message")?.jsonPrimitive?.contentOrNull

                                        player.sendMessage("§3[$fcName] $senderName:§f $text")
                                    }
                                }
                            } catch(_: Throwable) { }
                        }
                    }
                }
                player.sendMessage("§3Darkan §f- Successfully logged in as ${account["displayName"]}")
            } catch (e: Exception) {
                player.sendMessage("§cAn error occurred during the login process. Please try again later.")
                e.printStackTrace()
            }
    }

    fun join(player: Player) {
        launch {
            connectToWebsocket(player)
        }
    }

    fun quit(player: Player) {
        sessions[player.identity().uuid()]?.first?.close()
        sessions.remove(player.identity().uuid())
    }

    suspend fun post(endpoint: String, configure: HttpRequestBuilder.() -> Unit): HttpResponse {
        val response: HttpResponse = authClient.post {
            url(endpoint)
            configure()
        }
        return response
    }

    suspend fun get(endpoint: String, configure: HttpRequestBuilder.() -> Unit): HttpResponse {
        val response: HttpResponse = authClient.get {
            url(endpoint)
            configure()
        }
        return response
    }

    private fun initializeDatabase() {
        try {
            DriverManager.getConnection(dataDbUrl).use { connection ->
                connection.createStatement().use { statement ->
                    statement.execute("CREATE TABLE IF NOT EXISTS players (uuid TEXT PRIMARY KEY, authToken TEXT)")
                }
            }
        } catch (e: SQLException) {
            logger.severe("Could not initialize the database: ${e.message}")
        }
    }

    private suspend fun setAuthToken(uuid: String, authToken: String) {
        withContext(Dispatchers.IO) {
            try {
                DriverManager.getConnection(dataDbUrl).use { connection ->
                    connection.prepareStatement("INSERT INTO players (uuid, authToken) VALUES (?, ?) ON CONFLICT(uuid) DO UPDATE SET authToken = ?").use { statement ->
                        statement.setString(1, uuid)
                        statement.setString(2, authToken)
                        statement.setString(3, authToken)
                        statement.executeUpdate()
                    }
                }
            } catch (e: SQLException) {
                logger.severe("Failed to set auth token: ${e.message}")
            }
        }
    }

    private suspend fun getAuthToken(uuid: String): String? {
        val authToken = withContext(Dispatchers.IO) {
            var authToken: String? = null
            try {
                DriverManager.getConnection(dataDbUrl).use { connection ->
                    connection.prepareStatement("SELECT authToken FROM players WHERE uuid = ?").use { statement ->
                        statement.setString(1, uuid)
                        val resultSet = statement.executeQuery()
                        if (resultSet.next())
                            authToken = resultSet.getString("authToken")
                    }
                }
            } catch (e: SQLException) {
                logger.severe("Failed to get auth token: ${e.message}")
            }
            authToken
        }
        return authToken
    }
}